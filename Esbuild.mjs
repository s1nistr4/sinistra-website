import * as esbuild from 'esbuild'

let ctx = await esbuild.context({
    entryPoints: ['./static-dev/typescript/global.ts'],
    outfile: './static/js/global.js',
    bundle: true,
    minify: true, 
    sourcemap: false, 
    platform: "node",
    target: "es6"
})

await ctx.watch()
console.log('ESBuild is watching your code.')
