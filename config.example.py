class Config:
    DEBUG = True
    #SERVER_NAME = "s1nistr4.com"
    PORT = 1337
    MAINTENANCE = False
    STRFTIME_FORMAT = "%b%e, %Y %l:%M%p"
    DATABASE_TYPE = "postgres" # must be sqlite or postgres

    # SQlite 
    SQLALCHEMY_DATABASE_URI = "sqlite:///data.db"

    # PostgreSQL 
    """
    DB_USERNAME = "sinistra"
    DB_PASSWORD = "Alpine#12"
    DB_SERVER = "localhost" 
    DB_PORT = "5432"
    DB_NAME = "sinistrawebsite"

    SQLALCHEMY_DATABASE_URI = f"postgresql://{DB_USERNAME}:{DB_PASSWORD}@{DB_SERVER}:{DB_PORT}/{DB_NAME}"
    """

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAX_CONTENT_LENGTH = 16 * 1000 * 1000 # 16mb

    # Allowed image filetypes for file uploads
    IMG_EXT = ["jpg", "jpeg", "png", "gif"]
    IMG_MIME = ["image/jpg", "image/jpeg", "image/png", "image/gif"]

    # Form Lengths
    LEN  = {
        "title": 300,
        "content": 500,
        "blog": 50000,
        "url": 1000
    }

    # change before production
    SECRET_KEY = "penis"
    SECRET_ACCESS_COOKIE = "penis" # Set an HTTP header to access admin features, and access the website when it's under maintenance.
