from config import Config 
from database import db
from lib.generate_timestamp import generate_timestamp
from lib.generate_uuid import generate_uuid

class BlogPosts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(200), nullable=False, default=generate_uuid())
    timestamp = db.Column(db.DateTime, nullable=False, default=generate_timestamp())

    title = db.Column(db.String(Config.LEN['title']), nullable=False, default="Untitled")
    summary = db.Column(db.String(Config.LEN['blog']), nullable=False, default="No Summary")
    content = db.Column(db.String(Config.LEN['blog']), nullable=False, default="No blog content")

    def __repr__(self):
        return self.title
