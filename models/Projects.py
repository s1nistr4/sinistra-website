from config import Config 
from database import db
from lib.generate_timestamp import generate_timestamp
from lib.generate_uuid import generate_uuid

class Projects(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(200), nullable=False, default=generate_uuid())
    timestamp = db.Column(db.DateTime, nullable=False, default=generate_timestamp())

    url = db.Column(db.String(256))
    title = db.Column(db.String(Config.LEN['title']), nullable=False, default="")
    codename = db.Column(db.String(Config.LEN['title']), nullable=False, default="")
    project_type = db.Column(db.String(200), nullable=False, default="")
    project_category = db.Column(db.String(200), nullable=False, default="")
    project_image = db.Column(db.String(Config.LEN['url']))
    project_url = db.Column(db.String(Config.LEN['url']))
    description = db.Column(db.String(Config.LEN['content']), nullable=False, default="")
    featured = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return self.title
