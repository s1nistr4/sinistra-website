from models.Projects import Projects 

def return_projects():
    projects = Projects.query.order_by(Projects.title.asc()).all()

    return projects
