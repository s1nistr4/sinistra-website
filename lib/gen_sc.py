from config import Config 

def gen_sc(request):
    return {
        "header": request.cookies.get("token"), 
        "key": Config.SECRET_ACCESS_COOKIE
    }
