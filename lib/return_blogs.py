from models.BlogPosts import BlogPosts 
from markdown2 import markdown

def return_blogs() -> list:
    blog_posts = BlogPosts.query.order_by(BlogPosts.timestamp.desc()).all()
    blog_posts_formatted = []

    for post in blog_posts:
        markdown_as_html = markdown(post.content)

        blog_posts_formatted.append({
            "uid": post.uid,
            "timestamp": post.timestamp,
            "title": post.title,
            "summary": post.summary,
            "content": markdown_as_html 
        })

    return blog_posts_formatted
