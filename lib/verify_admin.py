from config import Config
from flask import request

def verify_admin():
    access_cookie = request.cookies.get("token", None)
    print(access_cookie)
    
    if access_cookie == None:
        return False
    elif access_cookie == Config.SECRET_ACCESS_COOKIE:
        return True
    else:
        return False
