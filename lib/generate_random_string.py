from random import choice 

def generate_random_string():
    output = ""
    chars = "qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPASDFGHJKLZXCVBNM"

    for i in range(32):
        output += choice(chars)

    return output
