from __future__ import annotations
from flask import Flask, render_template, request, redirect, url_for
from os.path import exists
from config import Config
from database import db
from lib.gen_sc import gen_sc
from lib.return_projects import return_projects
from flask_migrate import Migrate

# blueprints
from routes.blog import blog_blueprint
from routes.admin import admin_blueprint 
from routes.home import home_blueprint 
from routes.in_blog import in_blog_blueprint
from routes.project_route import projects_route_blueprint
from routes.signin import signin_blueprint

# bootstrapper
app = Flask(__name__)
app.config.from_object(Config)
db.init_app(app)
migrate = Migrate(app, db)

# register blueprints 
app.register_blueprint(blog_blueprint)
app.register_blueprint(admin_blueprint)
app.register_blueprint(home_blueprint)
app.register_blueprint(in_blog_blueprint)
app.register_blueprint(projects_route_blueprint)
app.register_blueprint(signin_blueprint)

# Models
if Config.DATABASE_TYPE == "sqlite":
    if not exists("instance/data.db"):
        with app.app_context():
            db.create_all()
        print("Created database!")
    else:
        print("Database already exists.")

elif Config.DATABASE_TYPE == "postgres": 
    with app.app_context():
        db.create_all()
        print("Created database!")

@app.before_request
def before_request():
    db.create_all()
    if Config.MAINTENANCE == True:
        access_cookie = request.cookies.get("token", None)

        if access_cookie != None:
            if access_cookie == Config.SECRET_ACCESS_COOKIE:
                pass

        return "<h1>site is down for now</h1>"

    else:
        pass

# Errorhandler
@app.errorhandler(403)
def error_403(e):
    return redirect(url_for("https://goolag.com/"))

@app.errorhandler(404)
def error_404(e) -> str:
    projects = return_projects()
    return render_template("404.html", projects=projects, sc=gen_sc(request))

# Run the App!
if __name__ == "__main__":
    app.run(port=Config.PORT)
