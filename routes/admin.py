from flask import Blueprint, render_template, request, redirect, url_for, flash, abort
from filetype import guess 
from lib.verify_admin import verify_admin
from lib.generate_uuid import generate_uuid 
from lib.generate_timestamp import generate_timestamp
from lib.generate_random_string import generate_random_string
from models.Projects import Projects
from os import getcwd
from models.BlogPosts import BlogPosts
from config import Config
from database import db 
from PIL import Image

admin_blueprint = Blueprint("admin", __name__)

@admin_blueprint.route("/admin/", methods=["GET", "POST"])
def admin():
    if request.method == "POST" and verify_admin() == True:
        # Project
        project_title = request.form.get("project-title", None)
        project_url = request.form.get("project-url", None)
        project_codename = request.form.get("project-codename", None)
        project_type = request.form.get("project-type", None)
        project_category = request.form.get("project-category", None)
        project_desc = request.form.get("project-desc", None)
        project_featured = request.form.get("project-featured", None)
        project_image = request.files.get("project-image", None)
        project_image_type = guess(project_image)

        # blog
        blog_title = request.form.get("blog-title", None)
        blog_summary = request.form.get("blog-summary", None)
        blog_content = request.form.get("blog-content", None)

        def project_handler() -> bool:
            if project_title != None and project_type != None and project_category != None and project_desc != None and project_featured != None: 
                if project_featured == "yes":
                    proj_featured_final = True
                else:
                    proj_featured_final = False


                # Capitalize the title
                capitalized_title = project_title.capitalize()

                if project_image_type != None:
                    if project_image_type.extension in Config.IMG_EXT and project_image_type.mime in Config.IMG_MIME:
                        filename = generate_random_string()

                        with Image.open(project_image) as img:
                            x = img.resize((80, 80))
                            x.save(f"{getcwd()}/static/images/submitted/{filename}.{project_image_type.extension}")

                        p = Projects(
                            uid=generate_uuid(), 
                            timestamp=generate_timestamp(), 
                            title=capitalized_title, 
                            codename=project_codename,
                            project_type=project_type, 
                            project_category=project_category, 
                            project_image=f"{filename}.{project_image_type.extension}",
                            project_url=project_url,
                            description=project_desc, 
                            featured=proj_featured_final
                        )
                    else:
                        flash("The image you submitted had an invalid extension and/or mimetype. Try submitting another one, or stop hacking.")
                        return redirect(url_for("admin.admin"))
                else:
                    p = Projects(
                        uid=generate_uuid(), 
                        timestamp=generate_timestamp(), 

                        title=capitalized_title, 
                        project_type=project_type, 
                        project_category=project_category, 
                        project_url=project_url,
                        description=project_desc, 
                        featured=proj_featured_final
                    )

                db.session.add(p)
                db.session.commit()

                return True
            else:
                return False

        def blog_handler() -> bool:
            if blog_title != None and blog_content != None and blog_summary != None:
                bp = BlogPosts(
                    uid=generate_uuid(), 
                    timestamp=generate_timestamp(), 
                    title=blog_title, 
                    summary=blog_summary, 
                    content=blog_content
                )

                db.session.add(bp)
                db.session.commit()

                return True

            else:
                return False

        if project_handler() == True or blog_handler() == True:
            flash("Action completed successfully.")
            return redirect(url_for("admin.admin"))

        else:
            flash("Action failed.")
            return redirect(url_for("admin.admin"))

    elif request.method == "GET" and verify_admin() == True:
        return render_template("admin.html")

    else:
        return abort(404)
