from flask import Blueprint, render_template, request, abort
from lib.gen_sc import gen_sc
from lib.return_projects import return_projects
from models.BlogPosts import BlogPosts
from markdown2 import markdown
from config import Config

in_blog_blueprint = Blueprint("in_blog", __name__)

# Inside blog
@in_blog_blueprint.route("/blog/<string:blog>/")
def in_blog(blog):
    blog_post = BlogPosts.query.filter_by(uid=blog).first()
    projects = return_projects()

    if blog_post != [None, ""]:
        markdown_as_html = markdown(blog_post.content)
        blog_post_formatted = {
            "uid": blog_post.uid,
            "timestamp": blog_post.timestamp,
            "title": blog_post.title,
            "content": markdown_as_html 
        }

        return render_template("in_blog.html", blog=blog_post_formatted, projects=projects, ts=Config.STRFTIME_FORMAT, sc=gen_sc(request))
    else:
        abort(404)
