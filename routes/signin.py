from flask import Blueprint, request, render_template, url_for, redirect, make_response
from config import Config
from lib.gen_sc import gen_sc
from lib.verify_admin import verify_admin
from datetime import datetime, timedelta

signin_blueprint = Blueprint("signin", __name__)

@signin_blueprint.route("/signin/", methods=["GET", "POST"])
def signin():
    if request.method == "POST": 
        if verify_admin() == False:
            token = request.form.get("token", None)

            if token != None: 
                if str(token) == Config.SECRET_ACCESS_COOKIE: 
                    resp = make_response(redirect(url_for("home.home")))
                    expire_date = datetime.now()
                    expire_date = expire_date + timedelta(days=90)

                    resp.set_cookie("token",
                        value=str(token), 
                        expires=expire_date
                    )

                    return resp


            return redirect(url_for("signin.signin"))
        else: 
            return redirect(url_for("home.home"))

    if verify_admin() == False:
        return render_template("signin.html", strf=Config.STRFTIME_FORMAT, sc=gen_sc(request))
    else:
        return redirect(url_for("home.home"))
