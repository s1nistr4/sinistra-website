from flask import Blueprint, request, render_template
from sqlalchemy.sql import desc 
from config import Config
from lib.gen_sc import gen_sc
from lib.return_blogs import return_blogs
from lib.return_projects import return_projects

home_blueprint = Blueprint("home", __name__)

@home_blueprint.route("/")
def home():
    projects = return_projects()
    blogs = return_blogs()

    return render_template("home.html", blogs=blogs, projects=projects, strf=Config.STRFTIME_FORMAT, sc=gen_sc(request))
