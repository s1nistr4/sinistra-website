from flask import Blueprint, request, render_template 
from lib.return_blogs import return_blogs 
from lib.return_projects import return_projects
from lib.gen_sc import gen_sc 
from config import Config

blog_blueprint = Blueprint("blog", __name__)

# Blog Posts
@blog_blueprint.route("/blog/")
def blog() -> str:
    blogs = return_blogs()
    projects = return_projects()
    return render_template("blog.html", blogs=blogs, projects=projects, ts=Config.STRFTIME_FORMAT, sc=gen_sc(request))
