from flask import Blueprint, render_template, request
from lib.gen_sc import gen_sc
from lib.return_projects import return_projects

projects_route_blueprint = Blueprint("projects_route", __name__)

@projects_route_blueprint.route("/projects/")
def projects_route() -> str:
    projects = return_projects()
    return render_template("projects.html", projects=projects, sc=gen_sc(request))
