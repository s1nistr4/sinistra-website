// sidebar
function sidebar():void {
    const sidebar:HTMLDivElement = document.querySelector<HTMLDivElement>(".sidebar")!;
    const sidebar_enter_button:HTMLElement = document.querySelector<HTMLElement>(".sidebar-enter-button")!;
    const sidebar_exit_button:HTMLElement = document.querySelector<HTMLElement>(".sidebar-exit-button")!;
    const sidebar_cover:HTMLElement = document.querySelector<HTMLElement>(".sidebar-cover")!;

    sidebar.style.transition = "500ms ease-out"

    function sidebar_open():void {
        sidebar.style.transform = "translateX(0px)"
        sidebar_cover.style.display = "block"
    }

    function sidebar_close():void {
        sidebar.style.transform = "translateX(-420px)"
        sidebar_cover.style.display = "none"
    }

    sidebar_enter_button.addEventListener("click", sidebar_open)
    sidebar_exit_button.addEventListener("click", sidebar_close)
    sidebar_cover.addEventListener("click", sidebar_close)
}

function cover():void {
    let cover_enter_button:HTMLElement = document.querySelector<HTMLElement>(".cover-enter-button")!
    let cover_exit_button:HTMLElement = document.querySelector<HTMLElement>(".cover-exit-button")!
    let cover:HTMLElement = document.querySelector<HTMLElement>(".cover")!

    console.log(cover_enter_button);
    console.log(cover_exit_button);


    cover_enter_button.addEventListener("click", ():void => {
        cover.style.display = "block"
    })

    cover_exit_button.addEventListener("click", ():void => {
        cover.style.display = "none"
    })
}

// generate a random color profile icon
function generate_profile_icon():void {
    const hue:number = Math.floor(Math.random() * 360)
    const profile_icon:HTMLImageElement|null = document.querySelector(".profile-icon")

    if (profile_icon != null) {
        profile_icon.style.filter = `hue-rotate(${hue}deg)`
    }
}

// When you click a link, run this function to play the animation first
function click_header_link():void {
    const header_links = document.querySelectorAll<HTMLElement>(".header-link")!

    for (let link of header_links) {
        link.addEventListener("click", ():void => {
            const url:string = link.getAttribute("data-link")!

            link.style.transform = "translateX(4px) translateY(4px)"
            link.style.boxShadow = "0px 0px 0px #2e2e2e"

            setTimeout(():void => {
                location.href = url
            }, 40)
        })
    }
}

function click_button():void {
    const button_links = document.querySelectorAll<HTMLElement>("button")!

    for (let button of button_links) {
        button.addEventListener("click", ():void => {
            const url:string = button.getAttribute("data-link")!

            setTimeout(():void => {
                window.open(url, '_blank');
            }, 40)
        })
    }
}

function welcome_animation() {
    const letters = document.querySelectorAll<HTMLElement>(".letter")

    if (letters.length != 0) {
        let index:number = 0

        setInterval(():void => {
            letters[index].style.color = "blue"
            index++
        }, 100)
    }
}


click_button()
click_header_link()
cover()
generate_profile_icon()
sidebar()
welcome_animation()
